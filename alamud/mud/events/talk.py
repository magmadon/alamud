from .event import Event2

class TalkEvent(Event2):
    NAME = "talk"
    def perform(self):
        self.buffer_clear()
        self.buffer_inform("talk.actor")
        self.actor.send_result(self.buffer_get())
