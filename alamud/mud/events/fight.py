from .event import Event2

class FightEvent(Event2):
    NAME = "inventory"

    def perform(self):
        verif1=False
        verif2=False
        self.buffer_clear()
        for x in self.actor.contents():
            if x.name=='armure':
                verif1=True
            if x.name=='arme':
                verif2=True
        if self.object.noun_the()=="le ragnaros" and self.object.has_prop("battu"):
            self.inform("fight.success")
        if self.object.noun_the()=="le ragnaros" and verif1 and verif2 and not self.object.has_prop("battu"):
            self.inform("fight")
            self.object.add_prop("battu")
        if self.object.noun_the()=="le ragnaros" and (not verif1 or not verif2) and not self.object.has_prop("battu"):
            self.inform("death")
            cont = self.actor.container()
            for x in list(self.actor.contents()):
                x.move_to(cont)
            self.actor.move_to(None)
        if self.object.noun_the()!="le ragnaros":
            self.fail()
            self.inform("fight.failed")
