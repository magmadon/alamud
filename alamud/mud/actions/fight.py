from .action import Action2
from mud.events import FightEvent

class FightAction(Action2):
    EVENT = FightEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "fight"
